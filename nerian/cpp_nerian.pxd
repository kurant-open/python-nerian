#ctypedef enum options: OPT1, OPT2, OPT3

from libcpp.vector cimport vector
from libcpp.string cimport string
from libcpp cimport bool


cdef extern from "visiontransfer/deviceinfo.h" namespace "visiontransfer":
    cdef cppclass DeviceInfo:
        DeviceInfo() except +
        string getIpAddress () except +
        string toString() except +
        string getFirmwareVersion() except +
 

cdef extern from "visiontransfer/deviceenumeration.h" namespace "visiontransfer":
    cdef cppclass DeviceEnumeration:
        DeviceEnumeration() except +
        vector[DeviceInfo] discoverDevices() except +

cdef extern from "visiontransfer/imageprotocol.h":
  cdef cppclass ProtocolType:
    pass


cdef extern from "visiontransfer/imageprotocol.h" namespace "visiontransfer":
  cdef ProtocolType PROTOCOL_TCP
  cdef ProtocolType PROTOCOL_UDP


cdef extern from "visiontransfer/imagepair.cpp":
    pass

cdef extern from "visiontransfer/imagepair.h" namespace "visiontransfer::ImagePair::ImageFormat":
    cdef enum ImageFormat "visiontransfer::ImagePair::ImageFormat":
        FORMAT_8_BIT_MONO
        FORMAT_8_BIT_RGB
        FORMAT_12_BIT_MONO


cdef extern from "visiontransfer/scenescanparameters.h" namespace "visiontransfer::SceneScanParameters::AutoMode":
    cdef enum AutoMode "visiontransfer::SceneScanParameters::AutoMode":
        AUTO_EXPOSURE_AND_GAIN
        AUTO_EXPOSURE_MANUAL_GAIN
        MANUAL_EXPOSURE_AUTO_GAIN
        MANUAL_EXPOSURE_MANUAL_GAIN


cdef extern from "visiontransfer/imagepair.h" namespace "visiontransfer":
    cdef cppclass ImagePair:
        ImagePair() except +
        int getWidth () except +
        int getHeight () except +
        const float * getQMatrix () except +
        void getTimestamp (int &seconds, int &microsec) except +
        int getBytesPerPixel (int imageNumber) except +
        bool isImageDisparityPair () except +
        int getSubpixelFactor () except +
        int getRowStride (int imageNumber) except +
        unsigned char * getPixelData (int imageNumber) except +
        ImageFormat getPixelFormat (int imageNumber) except +
        void writePgmFile(int imageNumber, const char* fileName) except +


cdef extern from "visiontransfer/imagetransfer.cpp":
    pass


cdef extern from "visiontransfer/imagetransfer.h" namespace "visiontransfer":
    cdef cppclass ImageTransfer:
        ImageTransfer(const DeviceInfo &, int, int) except +
 
        #~ImageTransfer() except +
 
        void setTransferImagePair(const ImagePair& imagePair) except +
     
        #TransferStatus transferData() except +
     
        bool receiveImagePair(ImagePair& imagePair) except +
     
        #bool receivePartialImagePair(ImagePair& imagePair, int& validRows, bool& complete) except +
     
        int getNumDroppedFrames() except +
     
        bool tryAccept() except +
     
        bool isConnected() except +
     
        void disconnect() except +
     
        string getRemoteAddress() const 

cdef extern from "visiontransfer/reconstruct3d.h" namespace "visiontransfer":
    cdef cppclass Reconstruct3D:
        Reconstruct3D() except +
        float * createPointMap (const unsigned short *dispMap, int width, int height, int rowStride, const float *q, unsigned short minDisparity) except +
        void projectSinglePoint (int imageX, int imageY, unsigned short disparity, const float *q, float &pointX, float &pointY, float &pointZ) except +
        float * createPointMap (const ImagePair &imagePair, unsigned short minDisparity) except +
        void writePlyFile (const char *file, const ImagePair &imagePair, double maxZ, bool binary) except +


cdef extern from "visiontransfer/scenescanparameters.h" namespace "visiontransfer":
    cdef cppclass SceneScanParameters:
        SceneScanParameters(const DeviceInfo &) except +
        int getOperationMode () except +
        #void setOperationMode(OperationMode mode) except +
        void setOperationMode(int mode) except +
        int getDisparityOffset () except +
        void setDisparityOffset (int offset) except +
        int getStereoMatchingP1 () except +
        void setStereoMatchingP1 (int p1) except +
        int getStereoMatchingP2 () except +
        void setStereoMatchingP2 (int p2) except +
        bool getMaskBorderPixelsEnabled () except +
        void setMaskBorderPixelsEnabled (bool enabled) except +
        bool getConsistencyCheckEnabled () except +
        void setConsistencyCheckEnabled (bool enabled) except +
        int getConsistencyCheckSensitivity () except +
        void setConsistencyCheckSensitivity (int sensitivity) except +
        bool getUniquenessCheckEnabled () except +
        void setUniquenessCheckEnabled (bool enabled) except +
        int getUniquenessCheckSensitivity () except +
        void setUniquenessCheckSensitivity (int sensitivity) except +
        bool getTextureFilterEnabled () except +
        void setTextureFilterEnabled (bool enabled) except +
        int getTextureFilterSensitivity () except +
        void setTextureFilterSensitivity (int sensitivity) except +
        bool getGapInterpolationEnabled () except +
        void setGapInterpolationEnabled (bool enabled) except +
        bool getNoiseReductionEnabled () except +
        void setNoiseReductionEnabled (bool enabled) except +
        int getSpeckleFilterIterations () except +
        void setSpeckleFilterIterations (int iter) except +
        AutoMode getAutoMode () except +
        void setAutoMode (AutoMode mode) except +
        double getAutoTargetIntensity () except +
        void setAutoTargetIntensity (double intensity) except +
        double getAutoIntensityDelta () except +
        void setAutoIntensityDelta (double delta) except +
        AutoMode getAutoTargetFrame () except +
        int getAutoTargetFrame () except +
        #void setAutoTargetFrame (TargetFrame target) except +
        void setAutoTargetFrame (int target) except +
        int getAutoSkippedFrames () except +
        void setAutoSkippedFrames (int skipped) except +
        double getAutoMaxExposureTime () except +
        void setAutoMaxExposureTime (double time) except +
        double getAutoMaxGain () except +
        void setAutoMaxGain (double gain) except +
        double getManualExposureTime () except +
        void setManualExposureTime (double time) except +
        double getManualGain () except +
        void setManualGain (double gain) except +
        bool getAutoROIEnabled () except +
        void setAutoROIEnabled (bool enabled) except +
        void getAutoROI (int &x, int &y, int &width, int &height) except +
        void setAutoROI (int x, int y, int width, int height) except +
        int getMaxFrameTimeDifference () except +
        void setMaxFrameTimeDifference (int diffMs) except +
        double getTriggerFrequency () except +
        void setTriggerFrequency (double freq) except +
        bool getTrigger0Enabled () except +
        void setTrigger0Enabled (bool enabled) except +
        bool getTrigger1Enabled () except +
        void setTrigger1Enabled (bool enabled) except +
        double getTrigger0PulseWidth (int pulse) except +
        void setTrigger0PulseWidth (double width) except +
        double getTrigger1PulseWidth (int pulse) except +
        void setTrigger1PulseWidth (double width) except +
        double getTrigger1Offset () except +
        void setTrigger1Offset (double offset) except +
        bool getAutoRecalibrationEnabled () except +
        void setAutoRecalibrationEnabled (bool enabled) except +
        bool getSaveAutoReclabration () except +
        void setSaveAutoReclabration (bool save) except +
