# distutils: language = c++
# cython: language_level=3


cimport cpp_nerian as cpp
from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp cimport bool
from cython cimport view

cimport numpy as np
import numpy as np
np.import_array()

from enum import IntEnum


class PixelFormat(IntEnum):
    FORMAT_8_BIT_MONO = 0
    FORMAT_8_BIT_RGB = 1
    FORMAT_12_BIT_MONO = 2


class AutoMode(IntEnum):
    AUTO_EXPOSURE_AND_GAIN = 0
    AUTO_EXPOSURE_MANUAL_GAIN = 1
    MANUAL_EXPOSURE_AUTO_GAIN = 2
    MANUAL_EXPOSURE_MANUAL_GAIN = 3

cdef class DeviceEnumeration:
    cdef cpp.DeviceEnumeration c_obj  # Hold a C++ instance which we're wrapping

    def discover_devices(self):
        mylist = []
        cdef vector[cpp.DeviceInfo] vect = self.c_obj.discoverDevices()
        for val in vect:
            di = DeviceInfo()
            di.c_obj = val
            mylist.append(di)
        return mylist


cdef class DeviceInfo:
    cdef cpp.DeviceInfo c_obj  # Hold a C++ instance which we're wrapping

    def get_ip_address(self):
        cdef string s = self.c_obj.getIpAddress()
        return s.decode("UTF-8")

    def __str__(self):
        cdef string s = self.c_obj.toString()
        return s.decode("UTF-8")

    __repr__ = __str__

    def get_firmaware_version(self):
        cdef string s = self.c_obj.getFirmwareVersion()
        return s.decode("UTF-8")



cdef class ImagePair:
    cdef cpp.ImagePair c_obj  # Hold a C++ instance which we're wrapping

    def get_timestamp(self):
        cdef int sec = 0
        cdef int msec = 0
        self.c_obj.getTimestamp(sec, msec)
        return sec, msec

    def get_width(self):
        return self.c_obj.getWidth()

    def get_height(self):
        return self.c_obj.getHeight()

    def __str__(self):
        w = self.get_width()
        h = self.get_height()
        return f"ImagePair({w}, {h})"

    __repr__ = __str__

    def get_qmatrix(self):
        cdef view.array ar = view.array(shape=(16, ), itemsize=sizeof(float), format="f", mode="c", allocate_buffer=False)
        cdef const float * pointer = self.c_obj.getQMatrix()
        ar.data = <char *> pointer
        np_array =  np.asarray(ar)
        np_array = np_array.reshape(4, 4)
        return np_array

    def get_pixel_format(self, int nb):
        return PixelFormat(self.c_obj.getPixelFormat(nb))

    def get_pixel_data(self, int nb):

        cdef int rowstride = self.c_obj.getRowStride(nb)
        cdef int w = self.c_obj.getWidth()
        cdef int h = self.c_obj.getHeight()
        cdef int size

        fmt = self.get_pixel_format(nb)
        if fmt == PixelFormat.FORMAT_12_BIT_MONO:
            #FIXME: might be wrong
            size = int(rowstride * h / 2)
            np_array = self._to_short_array(nb, size)
            np_array = np_array.reshape(h, int(rowstride/2))
            return np_array[:, :].copy()
        elif fmt == PixelFormat.FORMAT_8_BIT_RGB:
            #FIXME: might be wrong, not tested
            size = rowstride * h
            np_array = self._to_char_array(nb, size * 3)
            np_array = np_array.reshape(h, rowstride, 3)
            return np_array[:, :w, 3].copy()
        elif fmt == PixelFormat.FORMAT_8_BIT_MONO:
            size = rowstride * h
            np_array = self._to_char_array(nb, size)
            np_array = np_array.reshape(h, rowstride)
            return np_array[:, :w].copy()

    cdef _to_short_array(self, int nb, int size):
        cdef unsigned char * pointer = self.c_obj.getPixelData(nb)
        cdef np.uint16_t * short_prt = <np.uint16_t *> pointer
        cdef np.uint16_t[:] myview = <np.uint16_t[:size]> short_prt
        return np.asarray(myview)

    cdef _to_char_array(self, nb, size):
        cdef unsigned char * pointer = self.c_obj.getPixelData(nb)
        cdef np.uint8_t[:] char_view = <np.uint8_t[:size]> pointer
        return np.asarray(char_view)

    def get_disparity_map(self):
        return self.get_pixel_data(1)

    def get_image(self):
        return self.get_pixel_data(0)

    def get_point_cloud(self, max_z=10, min_disparity=1):
        cdef int w = self.c_obj.getWidth()
        cdef int h = self.c_obj.getHeight()
        cdef int size = w * h * 4
        cdef cpp.Reconstruct3D rec3d
        cdef float * pointer = rec3d.createPointMap(self.c_obj, min_disparity)

        cdef view.array arr = view.array(shape=(size,), itemsize=sizeof(float), format="f", mode="c", allocate_buffer=False)
        arr.data = <char*> pointer

        np_array = np.asarray(arr)
        np_array = np_array.reshape(h * w, 4)
        np_array = np_array[:, :3]
        if max_z > 0:
            np_array = np_array[np_array[:, 2] < max_z]
        return np_array

    def write_ply(self, name, double max_z=10, bool binary=False):
        cdef cpp.Reconstruct3D rec3d
        rec3d.writePlyFile(name.encode(), self.c_obj, max_z, binary)

    def write_pgm(self, name):
        self.c_obj.writePgmFile(0, name.encode())

    def get_row_stride(self, nb):
        return self.c_obj.getRowStride(nb)

    def get_bytes_per_pixel(self, nb):
        return self.c_obj.getBytesPerPixel(nb)

    def is_image_disparity_pair(self):
        return self.c_obj.isImageDisparityPair()

    def get_subpixel_factor(self):
        return self.c_obj.getSubpixelFactor()


cdef class ImageTransfer:
    cdef cpp.ImageTransfer*  c_obj  # Hold a C++ pointer

    def __cinit__(self, DeviceInfo device, int bufferSize=1048576, int maxUdpPacketSize=1472):
        self.c_obj = new cpp.ImageTransfer(device.c_obj, bufferSize, maxUdpPacketSize)

    def __dealloc__(self):
        del self.c_obj

    def is_connected(self):
        return self.c_obj.isConnected()

    def disconnect(self):
        self.c_obj.disconnect()

    def get_remote_address(self):
        cdef string s = self.c_obj.getRemoteAddress()
        return s.decode("UTF-8")

    def receive(self):
        imp = ImagePair()
        while not self.c_obj.receiveImagePair(imp.c_obj):
            pass
        return imp

    def try_accept(self):
        return self.c_obj.tryAccept()

    def get_num_dropped_frames(self):
        return self.c_obj.getNumDroppedFrames()

    def get_point_cloud(self, max_z):
        ip = self.receive()
        return ip.get_point_cloud(max_z)


cdef class Reconstruct3D:
    cdef cpp.Reconstruct3D c_obj  # Hold a C++ instance which we're wrapping

    def create_point_map_ip(self, ImagePair image_pair, max_z=10, min_disparity=1):

        cdef int w = image_pair.c_obj.getWidth()
        cdef int h = image_pair.c_obj.getHeight()
        cdef int size = w * h * 4
        cdef float * pointer = self.c_obj.createPointMap(image_pair.c_obj, min_disparity)

        cdef view.array arr = view.array(shape=(size,), itemsize=sizeof(float), format="f", mode="c", allocate_buffer=False)
        arr.data = <char*> pointer

        np_array = np.asarray(arr)
        np_array = np_array.reshape(h * w, 4)
        np_array = np_array[:, :3]

        if max_z > 0:
            np_array = np_array[np_array[:, 2] < max_z]

        return np_array


    def create_point_map_disparity(self, unsigned short[:, ::1] disparity_map, float[:, ::1] q, max_z=10, min_disparity=1):

        cdef int w = disparity_map.shape[1]
        cdef int h = disparity_map.shape[0]
        # cdef int size = w * h * 4

        cdef float[::1] arr = self._create_point_map(disparity_map, q, w, h, min_disparity)

        # cdef float * pointer = self.c_obj.createPointMap(&disparity_view[0, 0], w, h, w * 2, &q_view[0, 0], min_disparity)
        # cdef view.array arr = view.array(shape=(size,), itemsize=sizeof(float), format="f", mode="c", allocate_buffer=False)
        # arr.data = <char*> pointer

        np_array = np.asarray(arr)
        np_array = np_array.reshape(h * w, 3)

        if max_z > 0:
            np_array = np_array[np_array[:, 2] < max_z]

        return np_array


    cdef _create_point_map(self, unsigned short[:, ::1] disparity_view, float[:, ::1] q_view, width, height, min_disparity):

        cdef float[::1] point_map = np.zeros((width * height * 3,), dtype=np.float32)
        cdef float* output_ptr = &point_map[0];
        cdef double qx, qy, qz, qw
        cdef unsigned short int_disp
        cdef double d, w

        cdef unsigned short* disp_map = &disparity_view[0, 0]
        cdef float* q = &q_view[0, 0]


        for y in range(height):

            qx = q[1]*y + q[3]
            qy = q[5]*y + q[7]
            qz = q[9]*y + q[11]
            qw = q[13]*y + q[15]

            for x in range(width):

                int_disp = max(min_disparity, disp_map[y * width + x])

                if int_disp >= 0xFFF:
                    int_disp = min_disparity

                d = int_disp / 16.0
                w = qw + q[14]*d

                output_ptr[0] = <float>((qx + q[2]*d)/w) # x
                output_ptr[1] = <float>((qy + q[6]*d)/w) # y
                output_ptr[2] = <float>((qz + q[10]*d)/w) # z
                output_ptr += 3

                qx += q[0]
                qy += q[4]
                qz += q[8]
                qw += q[12]

        return point_map


    def project_single_point(self, point_x, point_y, disparity, q):

        cdef float x = 0
        cdef float y = 0
        cdef float z = 0
        cdef float[:, ::1] q_view = q

        self.c_obj.projectSinglePoint(point_x, point_y, disparity, &q_view[0, 0], x, y, z)
        return x, y, z


    def write_ply(self, ImagePair image_pair, string name, double max_z=10, bool binary=False):

        self.c_obj.writePlyFile(name.encode(), image_pair.c_obj, max_z, binary)


cdef class SceneScanParameters:
    cdef cpp.SceneScanParameters*  c_obj  # Hold a C++ pointer

    def __cinit__(self, DeviceInfo device_info):
        self.c_obj = new cpp.SceneScanParameters(device_info.c_obj)

    def __dealloc__(self):
        del self.c_obj

    def get_operation_mode(self):
        return self.c_obj.getOperationMode()

    def get_disparity_offset(self):
        return self.c_obj.getDisparityOffset()

    def set_disparity_offset(self, val):
        self.c_obj.setDisparityOffset (val)

    def get_auto_mode(self):
        return AutoMode(self.c_obj.getAutoMode())

    def set_auto_mode(self, cpp.AutoMode mode):
        self.c_obj.setAutoMode (mode)

    def get_auto_target_intensity(self):
        return self.c_obj.getAutoTargetIntensity ()

    def set_auto_target_intensity(self, double val):
        return self.c_obj.setAutoTargetIntensity(val)

    def get_auto_intensity_delta(self):
        return self.c_obj.getAutoIntensityDelta()

    def set_auto_intensity_delta(self, double val):
        self.c_obj.setAutoIntensityDelta(val)

    def get_auto_max_exposure_time(self):
        return self.c_obj.getAutoMaxExposureTime()

    def set_auto_max_exposure_time(self, double val):
        self.c_obj.setAutoMaxExposureTime(val)

    def get_manual_exposure_time(self):
        return self.c_obj.getManualExposureTime()

    def set_manual_exposure_time(self, double val):
        self.c_obj.setManualExposureTime(val)

    def get_manual_gain(self):
        return self.c_obj.getManualGain()

    def set_manual_gain(self, double val):
        self.c_obj.setManualGain(val)

    def get_auto_max_gain(self):
        return self.c_obj.getAutoMaxGain()

    def set_auto_max_gain(self, double val):
        self.c_obj.setAutoMaxGain(val)

    def set_trigger0_enabled(self, bool enabled):
        self.c_obj.setTrigger0Enabled(enabled)
        return self.c_obj.getTrigger0Enabled()

    def set_trigger_frequency(self, double freq):
        self.c_obj.setTriggerFrequency(freq)

    def get_trigger_frequency(self):
        return self.c_obj.getTriggerFrequency()

    def get_trigger0_pulse_width(self, int pulse):
        return self.c_obj.getTrigger0PulseWidth(pulse)

    def get_trigger1_pulse_width(self, int pulse):
        return self.c_obj.getTrigger1PulseWidth(pulse)

"""
        #AutoMode getAutoTargetFrame ()
        int getAutoTargetFrame ()
        #void setAutoTargetFrame (TargetFrame target)
        void setAutoTargetFrame (int target)
        int getAutoSkippedFrames ()
        void setAutoSkippedFrames (int skipped)
        bool getAutoROIEnabled ()
        void setAutoROIEnabled (bool enabled)
        void getAutoROI (int &x, int &y, int &width, int &height)
        void setAutoROI (int x, int y, int width, int height)
        int getMaxFrameTimeDifference ()
        void setMaxFrameTimeDifference (int diffMs)
        double getTriggerFrequency ()
        bool getTrigger1Enabled ()
        void setTrigger1Enabled (bool enabled)
        void setTrigger0PulseWidth (double width)
        double getTrigger1PulseWidth ()
        void setTrigger1PulseWidth (double width)
        double getTrigger1Offset ()
        void setTrigger1Offset (double offset)
        bool getAutoRecalibrationEnabled ()
        void setAutoRecalibrationEnabled (bool enabled)
        bool getSaveAutoReclabration ()
        void setSaveAutoReclabration (bool save)
"""
