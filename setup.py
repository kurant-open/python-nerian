
from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize

setup(
    name="python-nerian",
    packages=["nerian"],
    ext_modules=cythonize([
        Extension(
            name="nerian",
            sources=["nerian/nerian.pyx"],
            include_dirs=["../nerian/libvisiontransfer/"],
            libraries=["visiontransfer"],
            library_dirs=["../nerian/libvisiontransfer/lib/"],
            language="c++",
            #cmdclass = {'build_ext': build_ext}
        )

    ])
)
