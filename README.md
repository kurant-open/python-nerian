* download nerian cpp sdk source: https://nerian.com/support/software/downloads/
* tar -xf the_nerian_stuff_file.tar.xz
* cd nerian_xxxxxx/libvisiontransfer; cmake; make
* sudo make install
* sudo ldconfig  #update library list for linker
* cd ../..
* git clone git@gitlab.com:kurant-open/python-nerian.git
* cd python-nerian
* make   # or run manually `python setup.py build_ext --inplace`
* pip3 install --user -e . 
* ipython => import nerian, etc...


WARNING: In order to receive images from camera (over UDP) you need to set MTU to 9000. Try in user interface(in ubuntu/settings/Ethernt settings) and check it is correctly set using command line "ip addr | grep MTU".
also try to set it using manually if any issues: "sudo ip link set enxa0cec81d574a mtu 9000"


instead of installing libvisiontranfer you can also export a variable
* export LD_LIBRARY_PATH=../nerian/libvisiontransfer/lib/

FAQ:
* hanging in Transfer.receive(): Check that the web interface is not keeping the camera busy(close tab then), otherwise you probably have an MTU issue, see over
