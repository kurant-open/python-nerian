from IPython import embed
import nerian
import pc_viewer as pcv

import matplotlib.pyplot as plt

if __name__ == "__main__":
    # first discover device on network
    de = nerian.DeviceEnumeration()
    info_list = de.discover_devices()
    print("found cameras", info_list)
    device_info = info_list[0]
    print(device_info)
    # get a parmas object to evt change some settings on camera if not using the web interface
    params = nerian.SceneScanParameters(device_info)
    # make an ImageTrasnfer object for sync image transfer
    transfer = nerian.ImageTransfer(device_info)
    image_pair = transfer.receive()
    # get an image pair object
    print("ImagePair", image_pair)
    rgb = image_pair.get_image()
    # plt.showimg(rgb)
    depth = image_pair.get_disparity_map()
    # plt.showimg(depth)
    pc = image_pair.get_point_cloud()
    # alternative get directly a point cloud
    pc2 = transfer.get_point_cloud(10)  # arg is max_z in image coordinate
    # pcv.show(pc2)

    embed()
